using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocaCola.Pace.Cosmos;
using Microsoft.Extensions.Logging;
using PACE.Models;

namespace PACE.DataManagers
{
    public class TargetGroupManager : BaseDataManager<TargetGroup>
    {
        public TargetGroupManager(ILogger<TargetGroupManager> logger, IDocumentClientProvider provider) : base(logger, provider)
        {
        }

        public override string collectionId => "TargetGroup";

        /// <summary>
        /// Return a list of all key fields from target groups that have a model type of 'Distributor'.
        /// </summary>
        /// <returns>Return a DistributorUserModel List</returns>
        public async Task<List<DistributorUserModel>> GetAll()
        {
            try
            {
                // Query hardcoded to 'Distributor' model type.
                var query = from tg in await CreateDocumentQuery(queryAllPartitions)
                            where tg.ModelType == "Distributor"
                            select tg;

                List<DistributorUserModel> distribList = new List<DistributorUserModel>();
                
                // Convert Raw Query to DU Model
                foreach (var result in query.ToList())
                {
                    DistributorUserModel d = new DistributorUserModel
                    {
                        ModelType = result.ModelType,
                        id = result.id,
                        DisplayName = result.DisplayName,
                        ContainsAnyToken = result.ContainsAnyToken,
                        TokenField = result.TokenField,
                        FilterRules = result.FilterRules
                    };
                    distribList.Add(d);
                }

                return distribList;
            }
            catch (Exception e)
            {
                _logger.LogError(e, OOSConstants.FailedFind + OOSConstants.TargetGroup);
                throw e;
            }
        }
    }
}