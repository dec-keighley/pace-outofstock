using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocaCola.Pace.Cosmos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PACE.Models;

namespace PACE.DataManagers
{
    public class TargetingAdministratorsManager : BaseDataManager<TargetingAdministrators>
    {
        public TargetingAdministratorsManager(ILogger<TargetingAdministratorsManager> logger, IDocumentClientProvider provider) : base(logger, provider)
        {
        }

        public override string collectionId => "TargetingAdministrators";

        /// <summary>
        /// Returns the associated TargetGroupId entries based on a received UserId.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Return a string list of Id's</returns>
        public async Task<List<string>> GetTargetGroupsByUserId(string userId)
        {
            try
            {
                // Get All Query
                var query = from ta in await CreateDocumentQuery(queryAllPartitions)
                            where ta.UserId == userId
                            select ta;

                // Instantiate Model List
                List<string> associatedTargetGroups = new List<string>();

                // Convert Raw Query to DU Model
                foreach (var result in query.ToList())
                {
                    associatedTargetGroups.Add(result.TargetGroupId);
                }

                return associatedTargetGroups;
            }
            catch (Exception e)
            {
                _logger.LogError(e, OOSConstants.FailedFind + OOSConstants.TargetingAdministrator);
                throw e;
            }
        }

        /// <summary>
        /// Upsert a TargetingAdministrator document to cosmos.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="targetGroupId">Target Group Id</param>
        /// <returns>Returns the contents of the item added to cosmos.</returns>
        public async Task<string> UpsertTargetingAdministrator(string userId, string targetGroupId)
        {
            // Create new TargetingAdministrator
            TargetingAdministrators newTa = new TargetingAdministrators
            {
                id = userId + targetGroupId,
                UserId = userId,
                TargetGroupId = targetGroupId
            };

            // Upsert the document to cosmos.
            try
            {
                await UpsertDocument(newTa);
            }
            catch (Exception e)
            {
                _logger.LogError(e, OOSConstants.FailedAdd + OOSConstants.TargetingAdministrator);
                throw e;
            }

            return JsonConvert.SerializeObject(newTa);
        }

        /// <summary>
        /// Remove a targetingAdministrator entry from a cosmos container.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="targetGroupId">Target Group Id</param>
        /// <returns>Returns a string containing the ID</returns>
        public async Task<string> RemoveTargetingAdministrator(string userId, string targetGroupId)
        {
            // Establish the full ID of the document we want to remove.
            string fullId = userId + targetGroupId;

            // Pass full ID of the document and it's partition key
            try
            {
                await DeleteDocument(fullId, userId);
            }
            catch (Exception e)
            {
                _logger.LogError(e, OOSConstants.FailedRemove + OOSConstants.TargetingAdministrator);
                throw e;
            }

            return fullId;
        }
    }
}