using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocaCola.Pace.Cosmos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PACE.Models;

namespace PACE.DataManagers
{
    public class OutOfStockManager : BaseDataManager<PACE.Models.OutOfStock>
    {
        public OutOfStockManager(ILogger<OutOfStockManager> logger, IDocumentClientProvider provider) : base(logger, provider)
        {
        }

        public override string collectionId => "OutOfStock";

        public async Task<List<PACE.Models.OutOfStock>> GetAll()
        {
            // Get All Query
            var query = from oos in await CreateDocumentQuery(queryAllPartitions)
                        select oos;

            // Instantiate Model List
            List<PACE.Models.OutOfStock> associatedProducts = new List<PACE.Models.OutOfStock>();

            // Convert Raw Query to DU Model
            foreach (var result in query.ToList())
            {
                associatedProducts.Add(result);
            }

            return associatedProducts;
        }

        public async Task<List<string>> GetProductsByDistributorId(string distributorId)
        {
            try
            {
                // Get All Query
                var query = from oos in await CreateDocumentQuery(queryAllPartitions)
                            where oos.DistributorId == distributorId
                            select oos;

                // Instantiate Model List
                List<string> associatedProducts = new List<string>();

                // Convert Raw Query to DU Model
                foreach (var result in query.ToList())
                {
                    associatedProducts.Add(result.ProductId);
                }

                return associatedProducts;
            }
            catch (Exception e)
            {
                _logger.LogError(e, OOSConstants.FailedFind + OOSConstants.OutOfStock);
                throw e;
            }
        }

        public async Task<string> AddOutOfStock(string distributorId, string productId)
        {
            // New OutOfStock model
            PACE.Models.OutOfStock newOos = new PACE.Models.OutOfStock
            {
                id = distributorId + productId,
                DistributorId = distributorId,
                ProductId = productId
            };

            // Upsert the document to cosmos.
            try
            {
                await UpsertDocument(newOos);
            }
            catch (Exception e)
            {
                _logger.LogError(e, OOSConstants.FailedAdd + OOSConstants.OutOfStock);
                throw e;
            }

            return JsonConvert.SerializeObject(newOos);
        }

        public async Task<string> RemoveOutOfStock(string distributorId, string productId)
        {
            // Establish the full ID of the document we want to remove.
            string fullId = distributorId + productId;

            // Pass full ID of the document and it's partition key
            try
            {
                await DeleteDocument(fullId, distributorId);
            }
            catch (Exception e)
            {
                _logger.LogError(e, OOSConstants.FailedRemove + OOSConstants.OutOfStock);
                throw e;
            }

            return fullId;
        }
    }
}