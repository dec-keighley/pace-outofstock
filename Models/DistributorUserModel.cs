using System.Collections.Generic;
using PACE.Models;
/// <summary>
/// Model defining the contents of a distributor+user relationship.
/// </summary>
public class DistributorUserModel
{
    public string ModelType { get; set; }
    public string id { get; set; }
    public string DisplayName { get; set; }
    public List<string> ContainsAnyToken { get; set; }
    public string TokenField { get; set; }

    // Note: This list is using the PACE.Model FilterRule, not something defined in this project.
    public List<FilterRule> FilterRules { get; set; }
}