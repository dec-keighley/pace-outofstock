// Helper class which assists in connecting us to our Cosmos DB instance.
using System;
using System.Net.Http;
using CocaCola.Pace.Cosmos;
using CocaCola.Pace.Cosmos.Client;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.DependencyInjection;
using PACE.DataManagers;

public class Startup : FunctionsStartup
{
    public override void Configure(IFunctionsHostBuilder builder)
    {
        builder.Services.AddLogging();
        builder.Services.AddHttpClient();

        // Add a document client provider so we can access the Cosmos DB
        var brokerUri = Environment.GetEnvironmentVariable("PACE_COSMOS_DB_BROKER_URI", EnvironmentVariableTarget.Process);
        if (brokerUri == null || brokerUri.Length == 0)
        {
            builder.Services.AddDocumentClientProvider((s) =>
            {
                var dbEndpoint = Environment.GetEnvironmentVariable("CosmosDBStorageConnectionURL", EnvironmentVariableTarget.Process);
                var key = Environment.GetEnvironmentVariable("CosmosDBStorageReadOnlyKey", EnvironmentVariableTarget.Process);
                return new DocumentClientProvider.Configuration(new Uri(dbEndpoint), key);
            });
        }
        else
        {
            builder.Services.AddSingleton(_ =>
            {
                return new AzureServiceTokenProvider(); // Necessary to make default parameters work with DI
                });
            builder.Services.AddCosmosBrokerClient(_ =>
            {
                var brokerUser = Environment.GetEnvironmentVariable("PACE_COSMOS_DB_BROKER_USERNAME", EnvironmentVariableTarget.Process);
                var brokerResource = Environment.GetEnvironmentVariable("PACE_COSMOS_DB_BROKER_RESOURCE", EnvironmentVariableTarget.Process);
                var dbEndpoint = Environment.GetEnvironmentVariable("PACE_COSMOS_DB_ENDPOINT", EnvironmentVariableTarget.Process);
                var cosmosDbEndpoint = new Uri(dbEndpoint);
                return new ResourceBrokerClient.Configuration(brokerUri, brokerUser, brokerResource, cosmosDbEndpoint);
            });
        }

        builder.Services.AddScoped<TargetingAdministratorsManager>();
        builder.Services.AddScoped<TargetGroupManager>();
        builder.Services.AddScoped<OutOfStockManager>();
    }
}