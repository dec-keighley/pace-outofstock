public class OOSConstants {
    // Types
    public const string OutOfStock = "OutOfStock";
    public const string TargetGroup = "TargetGroup";
    public const string TargetingAdministrator = "TargetingAdministrator";

    // Strings
    public const string FailedAdd = "A request to add an object of the following type has failed: ";
    public const string FailedFind = "A request to find an object of the following type has failed: ";
    public const string FailedRemove = "A request to remove an object of the following type has failed: ";
}