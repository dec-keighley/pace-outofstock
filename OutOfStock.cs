using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using PACE.DataManagers;
using System.Collections.Generic;

[assembly: FunctionsStartup(typeof(Startup))]
namespace PACE.OutOfStock
{
    public class OutOfStock
    {
        TargetGroupManager _targetGroupManager;
        TargetingAdministratorsManager _targetingAdministratorsManager;
        OutOfStockManager _outOfStockManager;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="targetGroupManager">An instance of TargetGroupManager</param>
        /// <param name="targetingAdministratorsManager">Used for controlling CRUD operations, etc which 
        /// involve the TargetingAdministrators cosmos table.</param>
        /// <param name="outOfStockManager">OutOfStock Manager instance.</param>
        public OutOfStock(TargetGroupManager targetGroupManager,
                          TargetingAdministratorsManager targetingAdministratorsManager,
                          OutOfStockManager outOfStockManager)
        {
            this._targetGroupManager = targetGroupManager;
            this._targetingAdministratorsManager = targetingAdministratorsManager;
            this._outOfStockManager = outOfStockManager;
        }

        /// <summary>
        /// A simple function that takes no inputs, and yields a list of all distributor target groups on system.
        /// </summary>
        /// <returns>JSON Object containing all Distributors on the system.</returns>
        [FunctionName("GetAllDistributors")]
        public async Task<IActionResult> GetAllDistributors(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            // Use a datamanager instance to retrieve all distributor target groups.
            List<DistributorUserModel> distribGroups = await _targetGroupManager.GetAll();

            // Return
            return (ActionResult)new OkObjectResult(JsonConvert.SerializeObject(distribGroups));
        }

        /// <summary>
        /// A simple function that takes no inputs, and yields a list of all distrib/product pairs..
        /// </summary>
        /// <returns>JSON Object containing all OutOfStock distrib/product pairs. Use sparingly! Essentially a SELECT *</returns>
        [FunctionName("GetAllOutOfStock")]
        public async Task<IActionResult> GetAllOutOfStock(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            // Use a datamanager instance to retrieve all distributor target groups.
            List<PACE.Models.OutOfStock> outOfStockItems = await _outOfStockManager.GetAll();

            // Return
            return (ActionResult)new OkObjectResult(JsonConvert.SerializeObject(outOfStockItems));
        }

        /// <summary>
        /// Function which accepts a UserID and returns a list of distributors associated with it.
        /// </summary>
        /// <param name="userId">The user ID that we want to query.</param>
        /// <returns>A JSON object containing a list of distributors associated with a given UserID.</returns>
        [FunctionName("GetDistributorsForUser")]
        public async Task<IActionResult> GetDistributorsForUser(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "GetDistributorsForUser/{userId}")] HttpRequest req,
            string userId,
            ILogger log)
        {
            // Pull a list of distributors from the manager instance, and return it as json.
            List<string> distributorList = await _targetingAdministratorsManager.GetTargetGroupsByUserId(userId);
            return (ActionResult)new OkObjectResult(JsonConvert.SerializeObject(distributorList));
        }

        /// <summary>
        /// Function which accepts a DistributorId and returns a list of OutOfStock products associated with it.
        /// </summary>
        /// <param name="distributorId">The distributorId that we want to query.</param>
        /// <returns>A JSON object containing a list of OutOfStock products associated with a given distributor.</returns>
        [FunctionName("GetProductsForDistributor")]
        public async Task<IActionResult> GetProductsForDistributor(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "GetProductsForDistributor/{distributorId}")] HttpRequest req,
            string distributorId,
            ILogger log)
        {
            // Pull a list of distributors from the manager instance, and return it as json.
            List<string> productList = await _outOfStockManager.GetProductsByDistributorId(distributorId);
            return (ActionResult)new OkObjectResult(JsonConvert.SerializeObject(productList));
        }

        /// <summary>
        /// Upsert a new TargetingAdministrator entry to cosmos via a broker.
        /// </summary>
        /// <param name="targetingGroupId">The targetingGroupId that we want to add.</param>
        /// <param name="userId">The user ID that we want to add.</param>
        /// <returns>Returns a response object containing the contents of the added item.</returns>
        [FunctionName("UpsertTargetingAdministrator")]
        public async Task<IActionResult> UpsertTargetingAdministrator(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "UpsertTargetingAdministrator/{userId}/{targetGroupId}")] HttpRequest req,
            string userId,
            string targetGroupId,
            ILogger log)
        {
            // Returned object is a json string
            string addedTa = await _targetingAdministratorsManager.UpsertTargetingAdministrator(userId, targetGroupId);
            return (ActionResult)new OkObjectResult(addedTa);
        }

        /// <summary>
        /// Add a OutOfStock distrib/product pairing with the given IDs.
        /// </summary>
        /// <param name="distributorId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [FunctionName("AddOutOfStockProduct")]
        public async Task<IActionResult> AddOutOfStockProduct(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "AddOutOfStockProduct/{distributorId}/{productId}")] HttpRequest req,
            string distributorId,
            string productId,
            ILogger log)
        {
            // Returned object is a json string
            string addedOos = await _outOfStockManager.AddOutOfStock(distributorId, productId);
            return (ActionResult)new OkObjectResult(addedOos);
        }

        /// <summary>
        /// Remove a TargetingAdministrator pairing with the given IDs.
        /// </summary>
        /// <param name="distributorId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [FunctionName("RemoveTargetingAdministrator")]
        public async Task<IActionResult> RemoveTargetingAdministrator(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "RemoveTargetingAdministrator/{userId}/{targetGroupId}")] HttpRequest req,
            string userId,
            string targetGroupId,
            ILogger log)
        {
            // Returned object is a json string
            string removedTa = await _targetingAdministratorsManager.RemoveTargetingAdministrator(userId, targetGroupId);
            return (ActionResult)new OkObjectResult(removedTa);
        }

        /// <summary>
        /// Remove a OutOfStock distrib/product pairing with the given IDs.
        /// </summary>
        /// <param name="distributorId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        [FunctionName("RemoveOutOfStockProduct")]
        public async Task<IActionResult> RemoveOutOfStockProduct(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "RemoveOutOfStockProduct/{distributorId}/{productId}")] HttpRequest req,
            string distributorId,
            string productId,
            ILogger log)
        {
            // Returned object is a json string
            string removedOos = await _outOfStockManager.RemoveOutOfStock(distributorId, productId);
            return (ActionResult)new OkObjectResult(removedOos);
        }
    }
}
